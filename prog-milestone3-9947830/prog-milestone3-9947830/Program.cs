﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9947830
{
    class Program
    {

        // Main Menu //

        static void Main(string[] args)
        {
            Pizza myPizza = new Pizza();

            // Pizza Prices //
            myPizza.PepperoniSmall = "Small Pepperoni $5";
            myPizza.PepperoniMedium = "Medium Pepperoni $9";
            myPizza.PepperoniLarge = "Large Pepperoni $15";

            myPizza.MeatLoversSmall = "Small Meat Lovers $6";
            myPizza.MeatLoversMedium = "Medium Meat Lovers $10";
            myPizza.MeatLoversLarge = "Large MeatLovers $16";
          
            // Drink Prices //
            myPizza.Coke = "Coke $2";
            myPizza.Fanta = "Fanta $3";
            myPizza.Sprite = "Sprite $4";
            myPizza.OrangeJuice = "Orange Juice $1";

            Console.WriteLine("Hello and welcome to Saucy Pizza's");

            Client.myClient();
            Pizza.pizzaMenu();

            Console.WriteLine("Please select a pizza.");
            Console.WriteLine("Use number 0-5 for your selection.");
            /*
            string choice;
            choice = Console.ReadLine();

            while (choice != "6")
            {
                switch (choice)
                {
                    case "0":
                        Console.WriteLine(myPizza.PepperoniSmall);
                        break;
                    case "1":
                        Console.WriteLine(myPizza.PepperoniMedium);
                        break;
                    case "2":
                        Console.WriteLine(myPizza.PepperoniLarge);
                        break;
                    case "3":
                        Console.WriteLine(myPizza.MeatLoversSmall);
                        break;
                    case "4":
                        Console.WriteLine(myPizza.MeatLoversMedium);
                        break;
                    case "5":
                        Console.WriteLine(myPizza.MeatLoversLarge);
                        break;
                    default:
                        {
                            Console.WriteLine("Invalid entry. Please re-enter number");
                            break;
                        }
                    
                }
            }*/
 
            Console.ReadLine();
            Console.WriteLine("Would you like to select another pizza?");
            /*
            string choice1;
            choice1 = Console.ReadLine();

            while (choice1 != "6")
            {
                switch (choice1)
                {
                    case "0":
                        Console.WriteLine(myPizza.PepperoniSmall);
                        break;
                    case "1":
                        Console.WriteLine(myPizza.PepperoniMedium);
                        break;
                    case "2":
                        Console.WriteLine(myPizza.PepperoniLarge);
                        break;
                    case "3":
                        Console.WriteLine(myPizza.MeatLoversSmall);
                        break;
                    case "4":
                        Console.WriteLine(myPizza.MeatLoversMedium);
                        break;
                    case "5":
                        Console.WriteLine(myPizza.MeatLoversLarge);
                        break;
                    default:
                        {
                            Console.WriteLine("Invalid entry. Please re-enter number");
                            break;
                        }
                }
            } */
                            Console.ReadLine();

            Pizza.drinksMenu();

            Console.WriteLine("Would you like a drink?");
            Console.WriteLine("Please select a Drink.");
            Console.WriteLine("Use number 0-3 for your selection.");
            /*
            string choice2;
            choice2 = Console.ReadLine();

            while (choice2 != "4")
            {
                switch (choice2)
                {
                    case "0":
                        Console.WriteLine(myPizza.Coke);
                        break;
                    case "1":
                        Console.WriteLine(myPizza.Fanta);
                        break;
                    case "2":
                        Console.WriteLine(myPizza.Sprite);
                        break;
                    case "3":
                        Console.WriteLine(myPizza.OrangeJuice);
                        break;
                    default:
                        {
                            Console.WriteLine("Invalid entry. Please re-enter number");
                            break;
                        }

                }

            } */

                            Console.ReadLine();

        }
        //Bill
        public static void payment()
        {
            //Console.WriteLine("Enter in paying amount");
            //string payedAmount = Console.ReadLine();
            //(choice + choice1 + choice2) = (Total);
            //payedAmount - Total = Change
            //Console.WriteLine($"Name: {myName}");
            //Console.WriteLine($"Phone Number: {myPhoneNumber}");
            //Console.WriteLine($"Paid: {payedAmount}");
            //Console.WriteLine($"Total Amount: {Total}");
            //Console.WriteLine($"Change {Change}');

        }
    }
    class Pizza
    {
        public string PepperoniSmall { get; set; }
        public string PepperoniMedium { get; set; }
        public string PepperoniLarge { get; set; }
        public string MeatLoversSmall { get; set; }
        public string MeatLoversMedium { get; set; }
        public string MeatLoversLarge { get; set; }
        public string Coke { get; set; }
        public string Fanta { get; set; }
        public string Sprite { get; set; }
        public string OrangeJuice { get; set; }
        
        // Pizza Menu //
        public static void pizzaMenu()
        {
            Console.WriteLine("  ______________________________________  ");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** Pizza Menu:                          **");
            Console.WriteLine("**____________                          **");
            Console.WriteLine("**______________________________________**");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** Pepperoni.                           **");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** 0.                : Small   - $5.00  **");
            Console.WriteLine("** 1.                : Medium  - $9.00  **");
            Console.WriteLine("** 2.                : Large   - $15.00 **");
            Console.WriteLine("**______________________________________**");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** Meat Lovers.                         **");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** 3.                : Small   - $6.00  **");
            Console.WriteLine("** 4.                : Medium  - $10.00 **");
            Console.WriteLine("** 5.                : Large   - $16.00 **");
            Console.WriteLine("**______________________________________**");
        }

        public static void drinksMenu()
        {
            Console.Clear();
            Console.WriteLine("  ______________________________________  ");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** Drinks Menu:                         **");
            Console.WriteLine("**____________                          **");
            Console.WriteLine("**______________________________________**");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** 0. Coke:         - $2.00             **");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** 1. Fanta:        - $3.00             **");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** 2. Sprite:       - $4.00             **");
            Console.WriteLine("**                                      **");
            Console.WriteLine("** 3. Orange Juice: - $1.00             **");
            Console.WriteLine("**______________________________________**");
        }
    }
    class Client
    {
        // Client Details //
        public static void myClient()
        {
            Console.WriteLine("What is your name and phone number?");
            Console.Write("Type your name: ");
            string myName = Console.ReadLine();

            Console.Write("Type your phone number: ");
            string myPhoneNumber = Console.ReadLine();
         // clear page //
            Console.Clear();
            Console.WriteLine($"Name: {myName}");
            Console.WriteLine($"Phone Number: {myPhoneNumber}");
        }
    }
}




    
